var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CGrid = (function (_super) {
    __extends(CGrid, _super);
    function CGrid() {
        var _this = _super.call(this) || this;
        _this.m_txtId = new egret.TextField();
        _this.m_bmpEmpty = new egret.Bitmap(); //  该网格没有物件时显示的代图
        _this.m_BoundObj = null;
        _this.m_szId = "";
        _this.m_dicId = new Object();
        _this.m_objectPos = new Object();
        // this.m_Graphics = new egret.Shape();
        // this.addChild( this.m_Graphics );
        _this.m_bmpEmpty.texture = RES.getRes("dibiao-caodi_png");
        _this.addChild(_this.m_bmpEmpty);
        _this.addChild(_this.m_txtId);
        _this.m_txtId.text = "123";
        _this.m_txtId.size = 48;
        _this.m_txtId.x = _this.width / 2;
        _this.m_txtId.y = _this.height / 2;
        _this.m_txtId.textAlign = egret.HorizontalAlign.JUSTIFY;
        return _this;
    }
    //  绘制菱形（矢量图）
    CGrid.prototype.DrawRhombus = function (nWidth, nHeight, nHalfWidth, nHalfHeight) {
        /*
         this.m_Graphics.graphics.lineStyle(2, 0x000000 );
         this.m_Graphics.graphics.moveTo(0,nHalfHeight);
         this.m_Graphics.graphics.lineTo(nHalfWidth,nHeight);
          this.m_Graphics.graphics.lineTo(nWidth,nHalfHeight);
          this.m_Graphics.graphics.lineTo(nHalfWidth,0);
           this.m_Graphics.graphics.lineTo(0,nHalfHeight);
         this.m_Graphics.graphics.endFill();
         */
    };
    CGrid.prototype.SetId = function (nIndexX, nIndexY) {
        this.m_dicId["nIndexX"] = nIndexX;
        this.m_dicId["nIndexY"] = nIndexY;
    };
    CGrid.prototype.GetId = function () {
        return this.m_dicId;
    };
    CGrid.prototype.SetText = function (txt) {
        this.m_txtId.text = txt;
    };
    CGrid.prototype.SetBoundObj = function (obj) {
        this.m_BoundObj = obj;
    };
    CGrid.prototype.GetBoundObj = function (obj) {
        return this.m_BoundObj;
    };
    CGrid.prototype.SetPos = function (x, y) {
        this.x = x;
        this.y = y;
    };
    CGrid.prototype.GetPos = function () {
        this.m_objectPos["x"] = this.x;
        this.m_objectPos["y"] = this.y;
        return this.m_objectPos;
    };
    return CGrid;
}(egret.DisplayObjectContainer)); // end class CGrid
__reflect(CGrid.prototype, "CGrid");
//# sourceMappingURL=CGrid.js.map