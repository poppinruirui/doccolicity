//////////////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) 2014-present, Egret Technology.
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the Egret nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY EGRET AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
//  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL EGRET AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, DATA,
//  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//////////////////////////////////////////////////////////////////////////////////////
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var Main = (function (_super) {
    __extends(Main, _super);
    function Main() {
        var _this = _super.call(this) || this;
        _this.m_timerDragScene = new egret.Timer(300, 0);
        _this.m_bDraggingStage = false;
        _this.m_objectLastMousePosOnStage = new Object();
        _this.addEventListener(egret.Event.ADDED_TO_STAGE, _this.onAddToStage, _this);
        return _this;
    }
    Main.prototype.onAddToStage = function (event) {
        egret.lifecycle.addLifecycleListener(function (context) {
            // custom lifecycle plugin
            context.onUpdate = function () {
            };
        });
        egret.lifecycle.onPause = function () {
            egret.ticker.pause();
        };
        egret.lifecycle.onResume = function () {
            egret.ticker.resume();
        };
        this.runGame().catch(function (e) {
            console.log(e);
        });
    };
    Main.prototype.runGame = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result, userInfo;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        Main.s_nStageWidth = this.stage.stageWidth;
                        Main.s_nStageHeight = this.stage.stageHeight;
                        return [4 /*yield*/, this.loadResource()];
                    case 1:
                        _a.sent();
                        this.createGameScene();
                        return [4 /*yield*/, RES.getResAsync("description_json")];
                    case 2:
                        result = _a.sent();
                        this.startAnimation(result);
                        return [4 /*yield*/, platform.login()];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, platform.getUserInfo()];
                    case 4:
                        userInfo = _a.sent();
                        console.log(userInfo);
                        return [2 /*return*/];
                }
            });
        });
    };
    Main.prototype.loadResource = function () {
        return __awaiter(this, void 0, void 0, function () {
            var loadingView, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        loadingView = new LoadingUI();
                        this.stage.addChild(loadingView);
                        return [4 /*yield*/, RES.loadConfig("resource/default.res.json", "resource/")];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, RES.loadGroup("preload", 0, loadingView)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, RES.loadGroup("preload_audio", 0, loadingView)];
                    case 3:
                        _a.sent();
                        this.stage.removeChild(loadingView);
                        return [3 /*break*/, 5];
                    case 4:
                        e_1 = _a.sent();
                        console.error(e_1);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    Main.prototype.createGameScene = function () {
        this.stage.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.handleStageDown, this);
        this.stage.addEventListener(egret.TouchEvent.TOUCH_END, this.handleStageUp, this);
        this.stage.addEventListener(egret.TouchEvent.TOUCH_MOVE, this.handleStageMove, this);
        this.m_timerDragScene.addEventListener(egret.TimerEvent.TIMER, this.timerDragScene, this);
        this.stage.scaleMode = egret.StageScaleMode.SHOW_ALL;
        var bg = new egret.Shape();
        bg.graphics.beginFill(0x8F8F8F);
        bg.graphics.drawRect(0, 0, this.stage.stageWidth, this.stage.stageHeight);
        bg.graphics.endFill();
        this.addChild(bg);
        // 创建一个城镇
        this.m_CurTown = new CTown();
        this.addChild(this.m_CurTown);
        /*
               var tx:egret.TextField = new egret.TextField();
                tx.text = "欢迎进入神奇的多可粒世界!";
                tx.size = 32;
                this.addChild( tx );
               
                tx.touchEnabled = true;
                tx.addEventListener( egret.TouchEvent.TOUCH_TAP, this.handleTapTitle, this );
        */
        /*
                this.spore0 = new egret.Bitmap( RES.getRes("candy1_png") );
                 this.spore0.touchEnabled = true;
                this.spore0.x = 30;
                this.spore0.y = 30;
                this.addChild( this.spore0 );
                this.spore0.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.handleTapObj_Down, this );
                this.spore0.addEventListener(egret.TouchEvent.TOUCH_END, this.handleTapObj_Up, this );
        
                // 要有纹理，Bitmap对象才会显示内容
                 this.spore1 = new egret.Bitmap( RES.getRes("candy2_png") );
                 this.spore1.touchEnabled = true;
                this.spore1.x = 400;
                this.spore1.y = 30;
                this.addChild( this.spore1 );
                this.spore1.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.handleTapObj_Down, this );
                this.spore1.addEventListener(egret.TouchEvent.TOUCH_END, this.handleTapObj_Up, this );
                //this.spore1.scrollRect = new egret.Rectangle(0, 0, 32, 32);
        //this.spore1.width = 128;
        //this.spore1.height = 128;
        this.spore1.scaleX = 2;
        this.spore1.scaleY = 2;
        
        //this.spore0.mask = this.spore1;
        */
        this.stage.addEventListener(egret.TouchEvent.TOUCH_TAP, this.handleTapStage, this);
        // this._sound = RES.getRes( "wanjiashengji_wav" );
        /*
        var bigText: egret.TextField = new egret.TextField();
        bigText.x = 0;
        bigText.y = 0;
        bigText.text = this.spore1.width.toString() + "," + this.spore1.height.toString();
       
        bigText.cacheAsBitmap = true;
        this.addChild(bigText);
        
        this.timer = new egret.Timer(500,0);
        this.timer.addEventListener(egret.TimerEvent.TIMER,this.timerFunc,this);
        this.timer.start();
        */
    };
    Main.prototype.handleTapStage = function (evt) {
        // var channel:egret.SoundChannel = this._sound.play(0,1);
        /*
                egret.Tween.get( this.spore0 ).to( {x:this.spore1.x}, 1000, egret.Ease.sineIn );
                 egret.Tween.get( this.spore0 ).to( {alpha:0.1}, 500, egret.Ease.circIn ).to( {alpha:1}, 500, egret.Ease.circIn );
                egret.Tween.get( this.spore1 ).to( {x:this.spore0.x}, 1000, egret.Ease.sineIn );
                */
    };
    Main.prototype.handleTapTitle = function (evt) {
        var tx = evt.currentTarget;
        tx.textColor = 0xFF0000;
    };
    Main.prototype.timerDragScene = function () {
        console.log();
    };
    Main.prototype.handleStageDown = function (evt) {
        // this.m_timerDragScene.start();
        this.m_bDraggingStage = true;
        this.m_objectLastMousePosOnStage["x"] = evt.stageX;
        this.m_objectLastMousePosOnStage["y"] = evt.stageY;
    };
    Main.prototype.handleStageUp = function (evt) {
        //this.m_timerDragScene.stop();
        this.m_bDraggingStage = false;
    };
    Main.prototype.handleStageMove = function (evt) {
        var deltaX = evt.stageX - this.m_objectLastMousePosOnStage["x"];
        var deltaY = evt.stageY - this.m_objectLastMousePosOnStage["y"];
        // console.log( "Delat: " + deltaX + "_" + deltaY );  
        if (this.m_CurTown != null) {
            this.m_CurTown.DragScene(deltaX, deltaY);
        }
        this.m_objectLastMousePosOnStage["x"] = evt.stageX;
        this.m_objectLastMousePosOnStage["y"] = evt.stageY;
    };
    /**
     * 根据name关键字创建一个Bitmap对象。name属性请参考resources/resource.json配置文件的内容。
     * Create a Bitmap object according to name keyword.As for the property of name please refer to the configuration file of resources/resource.json.
     */
    Main.prototype.createBitmapByName = function (name) {
        var result = new egret.Bitmap();
        var texture = RES.getRes(name);
        result.texture = texture;
        return result;
    };
    /**
     * 描述文件加载成功，开始播放动画
     * Description file loading is successful, start to play the animation
     */
    Main.prototype.startAnimation = function (result) {
        var _this = this;
        var parser = new egret.HtmlTextParser();
        var textflowArr = result.map(function (text) { return parser.parse(text); });
        var textfield = this.textfield;
        var count = -1;
        var change = function () {
            count++;
            if (count >= textflowArr.length) {
                count = 0;
            }
            var textFlow = textflowArr[count];
            // 切换描述内容
            // Switch to described content
            textfield.textFlow = textFlow;
            var tw = egret.Tween.get(textfield);
            tw.to({ "alpha": 1 }, 200);
            tw.wait(2000);
            tw.to({ "alpha": 0 }, 200);
            tw.call(change, _this);
        };
        change();
    };
    Main.s_nStageWidth = 0;
    Main.s_nStageHeight = 0;
    return Main;
}(egret.DisplayObjectContainer));
__reflect(Main.prototype, "Main");
//# sourceMappingURL=Main.js.map