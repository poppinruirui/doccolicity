class CGrid extends egret.DisplayObjectContainer {

    private m_txtId:egret.TextField = new egret.TextField();
    private m_bmpEmpty:egret.Bitmap = new egret.Bitmap(); //  该网格没有物件时显示的代图

    private m_BoundObj:CObj = null;

      public constructor() {
        super();

       // this.m_Graphics = new egret.Shape();
       // this.addChild( this.m_Graphics );
        
        this.m_bmpEmpty.texture = RES.getRes("dibiao-caodi_png");
        this.addChild(this.m_bmpEmpty);

        this.addChild( this.m_txtId );
        this.m_txtId.text = "123";
        this.m_txtId.size  =48
        this.m_txtId.x = this.width / 2;
        this.m_txtId.y = this.height / 2;
        this.m_txtId.textAlign = egret.HorizontalAlign.JUSTIFY;
      }

      //  绘制菱形（矢量图）
      public DrawRhombus( nWidth:number, nHeight:number, nHalfWidth:number, nHalfHeight:number ):void
      {
        /*
         this.m_Graphics.graphics.lineStyle(2, 0x000000 );
         this.m_Graphics.graphics.moveTo(0,nHalfHeight);
         this.m_Graphics.graphics.lineTo(nHalfWidth,nHeight);
          this.m_Graphics.graphics.lineTo(nWidth,nHalfHeight);
          this.m_Graphics.graphics.lineTo(nHalfWidth,0);
           this.m_Graphics.graphics.lineTo(0,nHalfHeight);
         this.m_Graphics.graphics.endFill();  
         */
      }

      private m_szId:string = "";
     private m_dicId:Object = new Object();

      public SetId( nIndexX:number, nIndexY:number ):void
      {
        this.m_dicId["nIndexX"] = nIndexX;
         this.m_dicId["nIndexY"] = nIndexY;
      }

      public GetId(  ):Object
      {
          return this.m_dicId; 
      }



   

      public SetText( txt:string ):void
      {
         this.m_txtId.text = txt;
      }

      public SetBoundObj( obj:CObj ):void
      {
        this.m_BoundObj = obj;
      }

      public GetBoundObj( obj:CObj ):CObj
      {
        return this.m_BoundObj;
      }

 public SetPos( x:number, y:number ):void
    {
        this.x = x;
        this.y = y;
    }

    private m_objectPos:Object = new Object();
    public GetPos( ):Object
    {
        this.m_objectPos["x"] = this.x;
        this.m_objectPos["y"] = this.y;
        return this.m_objectPos;
    }

} // end class CGrid