
class CTown extends egret.DisplayObjectContainer {

private m_containerGrids:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
private m_containerObjs:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();

private m_dicGrids:Object = new Object();

public constructor() {
        super();

this.scaleX = 0.2;
this.scaleY = 0.2;

        this.addChild( this.m_containerGrids);
       
       
        this.addChild( this.m_containerObjs );
      

          var town_center:egret.Bitmap = new egret.Bitmap( RES.getRes("town_center_png") );
         town_center.touchEnabled = false;
        town_center.x = 0;
        town_center.y = 0;
        town_center.anchorOffsetX = town_center.width / 2;
        town_center.anchorOffsetY = town_center.height / 2;
        town_center.scaleX = 0.3;
        town_center.scaleY = 0.3;
        this.addChild( town_center );

       this.CreateGrids();
/*
      var spore0:egret.Bitmap = new egret.Bitmap( RES.getRes("candy1_png") );
         spore0.touchEnabled = true;
        spore0.x = 30;
        spore0.y = 30;
        this.addChild( spore0 );
        */
}



public CreateGrids():void
{
        var nWidth:number = CDef.s_nTileWidth;
        var nHeight:number = CDef.s_nTileHeight;
        var nHalfWidth:number = CDef.s_nTileWidth / 2;
        var nHalfHeight:number = CDef.s_nTileHeight / 2;

for ( var i:number = -10; i <= 10; i++ ) // y轴方向（行）
{
        for ( var j:number = -5; j <= 5; j++ )// x轴方向（列）
        {
                var posX:number = 0;
                var posY:number = 0;
                
                 if ( i % 2 == 0 ) // 偶数行
                  {
                       posX = j * nWidth;
                       posY = i * nHalfHeight; 
                  }
                  else// 奇数行
                  {
                        if( j > 0 )
                        {
                                posX = (j - 0.5) * nWidth;
                        }
                        else if ( j < 0 )
                        {
                                posX = (j + 0.5) * nWidth;
                        }
                        else
                        {
                                continue;
                        }
                        posY = i * nHalfHeight; 
                  }

                  var grid:CGrid = new CGrid();
                  this.m_containerGrids.addChild( grid );
                  grid.touchEnabled = true;
                  grid.anchorOffsetX = nHalfWidth;
                  grid.anchorOffsetY = CDef.s_nTileHeight;
                 // grid.DrawRhombus( CDef.s_nTileWidth, CDef.s_nTileHeight, nHalfWidth, nHalfHeight);
                  grid.x = posX;
                  grid.y = posY;

                  // j是x轴坐标，i是y轴坐标
                  var szKey:string = j + "," + i;
                  grid.SetText( szKey);
                  grid.SetId( j,i );
                  this.m_dicGrids[szKey] = grid;

                  grid.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.handleTapGrid_Begin, this );
                  grid.addEventListener(egret.TouchEvent.TOUCH_TAP, this.handleTapGrid, this );
               
        } // end for j
} // end for i        
} // end CreateGrids

public GetGridByIndex( nIndexX:number, nIndexY:number ):CGrid
{
        var szKey:string = nIndexX + "," + nIndexY;
        var grid:CGrid = this.m_dicGrids[szKey];      
        return grid;
}


private m_fTapBeginMouseX:number = 0;
private m_fTapBeginMouseY:number = 0;
private handleTapGrid_Begin( evt:egret.TouchEvent ):void
{
        this.m_fTapBeginMouseX = evt.stageX;
        this.m_fTapBeginMouseY = evt.stageY;       
}

private handleTapGrid( evt:egret.TouchEvent ):void
{       
 if ( ( this.m_fTapBeginMouseX !=  evt.stageX )||( this.m_fTapBeginMouseY !=  evt.stageY ) )
 {
        console.log( "滑动了，不算点击：" );
        return;

 }   

 var the_target:any  = evt.currentTarget;
 var grid:CGrid = the_target;
 var dicId:Object = grid.GetId();
 var nIndexX = dicId["nIndexX"];
 var nIndexY = dicId["nIndexY"];
 var tapped_grid:CGrid = null;
 var bHit = grid.hitTestPoint( evt.stageX ,evt.stageY, true );
 if ( bHit )
 {
        // console.log( "点中：" + grid.GetId()["nIndexX"] + "_" + grid.GetId()["nIndexY"]  );
         tapped_grid = grid;
 }
 else
 {
         var nIndexX_ZuoShang = 0;
         if ( nIndexY % 2 == 0 ) // 偶数行
         {
                nIndexX_ZuoShang = nIndexX;
         }
         else
         {
                nIndexX_ZuoShang = nIndexX - 1;
         }
         var nIndexY_ZuoShang = nIndexY - 1;
         grid = this.GetGridByIndex( nIndexX_ZuoShang, nIndexY_ZuoShang );
         if ( grid != null )
         {
              bHit = grid.hitTestPoint( evt.stageX ,evt.stageY, true );
              if ( bHit )
              {
                      // console.log( "点中：" + grid.GetId()["nIndexX"] + "_" + grid.GetId()["nIndexY"]  );
                       tapped_grid = grid;
              }  
              else
              {
                        var nIndexX_YouShang:number = 0;
                        var nIndexY_YouShang:number = nIndexY - 1;
                        if ( nIndexY % 2 == 0 ) // 偶数行
                        {
                                nIndexX_YouShang = nIndexX + 1;
                        }
                        else
                        {
                                nIndexX_YouShang = nIndexX;
                        }
                        grid = this.GetGridByIndex( nIndexX_YouShang, nIndexY_YouShang );
                        if ( grid != null )
                        {  
                                bHit = grid.hitTestPoint( evt.stageX ,evt.stageY, true );
                                if ( bHit )
                                {
                                        tapped_grid = grid;
                                            //    console.log( "点中：" + grid.GetId()["nIndexX"] + "_" + grid.GetId()["nIndexY"]  );
                                }  
                        } 
              }
         }
 }
 
if ( tapped_grid != null )
{
        console.log( "点中：" + tapped_grid.GetId()["nIndexX"] + "_" + tapped_grid.GetId()["nIndexY"] );
}
else
{
        console.log( "什么都没点中" );
}
 /*
    var dic:Object = grid.GetPos();
    var obj:CObj = CObjManager.NewObjByObjFuncAndResId( Global.eObjFunc.road, 0 );
    obj.anchorOffsetX = obj.width / 2;
     obj.anchorOffsetY = obj.height;
    this.InsertObj( obj );
    obj.SetPos( dic["x"], dic["y"] );
    */
}

// 这里用“插入排序法”
private InsertObj( obj:CObj ):void
{
this.m_containerObjs.addChild(obj);
}

public  DragScene( fDeltaX:number, fDeltaY:number ):void
{
        this.x += fDeltaX;
        this.y += fDeltaY;
}

} // end class CTown