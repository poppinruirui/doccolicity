
class CObjManager extends egret.DisplayObjectContainer {

    public static s_aryRoadResName:string[] = 
    [
        "blueRoad0_png",
        "blueRoad1_png"

    ];
   

    public static NewObj():CObj
    {
        return new CObj();
    }

    public static DeleteObj( obj:CObj ):void
    {
        
    }

   
    public static NewObjByObjFuncAndResId(  func_type:Global.eObjFunc, id:number ):CObj
    {
        var obj:CObj = CObjManager.NewObj();
        var szResName:string = "";
        switch( func_type )
        {
            case Global.eObjFunc.road:
            {
               szResName = CObjManager.s_aryRoadResName[id];   
            }
            break;
        } // end switch
        var tex:egret.Texture =  RES.getRes( szResName );
        obj.SetTexture(tex );
        return obj;
    }


} // end CObjManager