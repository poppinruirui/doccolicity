//////////////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) 2014-present, Egret Technology.
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the Egret nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY EGRET AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
//  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL EGRET AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, DATA,
//  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//////////////////////////////////////////////////////////////////////////////////////

class Main extends egret.DisplayObjectContainer {

    public constructor() {
        super();
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.onAddToStage, this);
    }

    private onAddToStage(event: egret.Event) {

        egret.lifecycle.addLifecycleListener((context) => {
            // custom lifecycle plugin

            context.onUpdate = () => {

            }
        })

        egret.lifecycle.onPause = () => {
            egret.ticker.pause();
        }

        egret.lifecycle.onResume = () => {
            egret.ticker.resume();
        }

        this.runGame().catch(e => {
            console.log(e);
        })



    }

    public static s_nStageWidth:number = 0;
    public static s_nStageHeight:number = 0;

    private async runGame() {

        Main.s_nStageWidth = this.stage.stageWidth;
        Main.s_nStageHeight = this.stage.stageHeight;
        await this.loadResource()
        this.createGameScene();
        const result = await RES.getResAsync("description_json")
        this.startAnimation(result);
        await platform.login();
        const userInfo = await platform.getUserInfo();
        console.log(userInfo);

    }

    private async loadResource() {
        try {
            const loadingView = new LoadingUI();
            this.stage.addChild(loadingView);
            await RES.loadConfig("resource/default.res.json", "resource/");
            await RES.loadGroup("preload", 0, loadingView);
            await RES.loadGroup("preload_audio", 0, loadingView);
            this.stage.removeChild(loadingView);
        }
        catch (e) {
            console.error(e);
        }
    }

    private textfield: egret.TextField;
    private spore0:egret.Bitmap;
    private spore1:egret.Bitmap;
    /**
     * 创建游戏场景
     * Create a game scene
     */

    private m_CurTown:CTown; // 当前正在编辑或浏览的城镇

    private createGameScene() {

    this.stage.addEventListener( egret.TouchEvent.TOUCH_BEGIN, this.handleStageDown, this )
      this.stage.addEventListener( egret.TouchEvent.TOUCH_END, this.handleStageUp, this )
      this.stage.addEventListener( egret.TouchEvent.TOUCH_MOVE, this.handleStageMove, this )
   this.m_timerDragScene.addEventListener(egret.TimerEvent.TIMER,this.timerDragScene,this);

      this.stage.scaleMode = egret.StageScaleMode.SHOW_ALL;
      var bg:egret.Shape = new egret.Shape();
       bg.graphics.beginFill( 0x8F8F8F );
       bg.graphics.drawRect( 0, 0, this.stage.stageWidth, this.stage.stageHeight ); 
       bg.graphics.endFill();  
       this.addChild( bg );
      // 创建一个城镇
      this.m_CurTown = new CTown();
     
      this.addChild( this.m_CurTown );

      

       
/*
       var tx:egret.TextField = new egret.TextField();
        tx.text = "欢迎进入神奇的多可粒世界!"; 
        tx.size = 32; 
        this.addChild( tx );
       
        tx.touchEnabled = true;        
        tx.addEventListener( egret.TouchEvent.TOUCH_TAP, this.handleTapTitle, this );
*/
/*
        this.spore0 = new egret.Bitmap( RES.getRes("candy1_png") );
         this.spore0.touchEnabled = true;
        this.spore0.x = 30;
        this.spore0.y = 30;
        this.addChild( this.spore0 );
        this.spore0.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.handleTapObj_Down, this );
        this.spore0.addEventListener(egret.TouchEvent.TOUCH_END, this.handleTapObj_Up, this );

        // 要有纹理，Bitmap对象才会显示内容
         this.spore1 = new egret.Bitmap( RES.getRes("candy2_png") );
         this.spore1.touchEnabled = true;
        this.spore1.x = 400;
        this.spore1.y = 30;
        this.addChild( this.spore1 );
        this.spore1.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.handleTapObj_Down, this );
        this.spore1.addEventListener(egret.TouchEvent.TOUCH_END, this.handleTapObj_Up, this );
        //this.spore1.scrollRect = new egret.Rectangle(0, 0, 32, 32);
//this.spore1.width = 128;
//this.spore1.height = 128;
this.spore1.scaleX = 2;
this.spore1.scaleY = 2;

//this.spore0.mask = this.spore1;
*/
        this.stage.addEventListener( egret.TouchEvent.TOUCH_TAP, this.handleTapStage, this );

       // this._sound = RES.getRes( "wanjiashengji_wav" );

        /*
        var bigText: egret.TextField = new egret.TextField();
        bigText.x = 0;
        bigText.y = 0;
        bigText.text = this.spore1.width.toString() + "," + this.spore1.height.toString();
       
        bigText.cacheAsBitmap = true;
        this.addChild(bigText);
        
        this.timer = new egret.Timer(500,0);
        this.timer.addEventListener(egret.TimerEvent.TIMER,this.timerFunc,this);
        this.timer.start();
        */
    }
    private timer:egret.Timer;

    

    private _sound:egret.Sound;
    

    private handleTapStage( evt:egret.TouchEvent ):void
    {
       // var channel:egret.SoundChannel = this._sound.play(0,1);
/*
        egret.Tween.get( this.spore0 ).to( {x:this.spore1.x}, 1000, egret.Ease.sineIn );
         egret.Tween.get( this.spore0 ).to( {alpha:0.1}, 500, egret.Ease.circIn ).to( {alpha:1}, 500, egret.Ease.circIn );
        egret.Tween.get( this.spore1 ).to( {x:this.spore0.x}, 1000, egret.Ease.sineIn );
        */
    }

    private handleTapTitle( evt:egret.TouchEvent ):void
    {
        var tx:egret.TextField = evt.currentTarget;
        tx.textColor = 0xFF0000;
    }



     private m_timerDragScene:egret.Timer = new egret.Timer( 300, 0 ) ;
 
    private timerDragScene()
    {
        console.log();
    }

    private m_bDraggingStage:boolean = false;
    private m_objectLastMousePosOnStage:Object = new Object();
    private handleStageDown( evt:egret.TouchEvent ):void
    {
      // this.m_timerDragScene.start();
      this.m_bDraggingStage = true;
      this.m_objectLastMousePosOnStage["x"] = evt.stageX;
      this.m_objectLastMousePosOnStage["y"] = evt.stageY;
    }

    private handleStageUp( evt:egret.TouchEvent ):void
    {
        //this.m_timerDragScene.stop();
         this.m_bDraggingStage = false;
    }

    private handleStageMove( evt:egret.TouchEvent ):void
    {
        var deltaX:number = evt.stageX - this.m_objectLastMousePosOnStage["x"];
        var deltaY:number = evt.stageY - this.m_objectLastMousePosOnStage["y"];
        
      // console.log( "Delat: " + deltaX + "_" + deltaY );  
      if ( this.m_CurTown != null )
      {
          this.m_CurTown.DragScene( deltaX, deltaY );
      }

        this.m_objectLastMousePosOnStage["x"] = evt.stageX;
      this.m_objectLastMousePosOnStage["y"] = evt.stageY;
    }

    /**
     * 根据name关键字创建一个Bitmap对象。name属性请参考resources/resource.json配置文件的内容。
     * Create a Bitmap object according to name keyword.As for the property of name please refer to the configuration file of resources/resource.json.
     */
    private createBitmapByName(name: string) {
        let result = new egret.Bitmap();
        let texture: egret.Texture = RES.getRes(name);
        result.texture = texture;
        return result;
    }

    /**
     * 描述文件加载成功，开始播放动画
     * Description file loading is successful, start to play the animation
     */
    private startAnimation(result: string[]) {
        let parser = new egret.HtmlTextParser();

        let textflowArr = result.map(text => parser.parse(text));
        let textfield = this.textfield;
        let count = -1;
        let change = () => {
            count++;
            if (count >= textflowArr.length) {
                count = 0;
            }
            let textFlow = textflowArr[count];

            // 切换描述内容
            // Switch to described content
            textfield.textFlow = textFlow;
            let tw = egret.Tween.get(textfield);
            tw.to({ "alpha": 1 }, 200);
            tw.wait(2000);
            tw.to({ "alpha": 0 }, 200);
            tw.call(change, this);
        };

        change();
    }
}