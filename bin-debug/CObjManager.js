var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CObjManager = (function (_super) {
    __extends(CObjManager, _super);
    function CObjManager() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CObjManager.NewObj = function () {
        return new CObj();
    };
    CObjManager.DeleteObj = function (obj) {
    };
    CObjManager.NewObjByObjFuncAndResId = function (func_type, id) {
        var obj = CObjManager.NewObj();
        var szResName = "";
        switch (func_type) {
            case Global.eObjFunc.road:
                {
                    szResName = CObjManager.s_aryRoadResName[id];
                }
                break;
        } // end switch
        var tex = RES.getRes(szResName);
        obj.SetTexture(tex);
        return obj;
    };
    CObjManager.s_aryRoadResName = [
        "blueRoad0_png",
        "blueRoad1_png"
    ];
    return CObjManager;
}(egret.DisplayObjectContainer)); // end CObjManager
__reflect(CObjManager.prototype, "CObjManager");
//# sourceMappingURL=CObjManager.js.map