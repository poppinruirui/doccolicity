module Global{

    export enum eObjType{
        hori, // 水平物件 ，如：路面、草坪、地砖
        vert, // 垂直物件，如：房屋、树木、风车、汽车
    }

    export enum eObjFunc{
        road, // 道路
        building, // 建筑物
        car, //汽车
        plant, // 植物
    }
}

class CObj extends egret.DisplayObjectContainer {

 public constructor() {
        super();

        this.addChild( this.m_bmpMainPic );
 }

    private m_eObjType:Global.eObjType; // Obj的类型：水平物件还是垂直物件
    public SetObjType( eObjType:Global.eObjType ):void
    {
        this.m_eObjType = eObjType;
    }

    public GetObjType():Global.eObjType
    {
        return this.m_eObjType;
    }

    private m_bmpMainPic:egret.Bitmap = new egret.Bitmap(); // 一个Obj的核心图片

    public SetPos( x:number, y:number ):void
    {
        this.x = x;
        this.y = y;
    }

    private m_objectPos:Object = new Object();
    public GetPos( ):Object
    {
        this.m_objectPos["x"] = this.x;
        this.m_objectPos["y"] = this.y;
        return this.m_objectPos;
    }

    public SetBitmap( bitmap:egret.Bitmap ):void
    {
        this.m_bmpMainPic = bitmap;

        
    }

    public SetTexture( tex:egret.Texture ):void
    {
        this.m_bmpMainPic.texture = tex;
    }

} // end class CObj