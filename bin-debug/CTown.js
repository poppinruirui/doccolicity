var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CTown = (function (_super) {
    __extends(CTown, _super);
    function CTown() {
        var _this = _super.call(this) || this;
        _this.m_containerGrids = new egret.DisplayObjectContainer();
        _this.m_containerObjs = new egret.DisplayObjectContainer();
        _this.m_dicGrids = new Object();
        _this.m_fTapBeginMouseX = 0;
        _this.m_fTapBeginMouseY = 0;
        _this.scaleX = 0.2;
        _this.scaleY = 0.2;
        _this.addChild(_this.m_containerGrids);
        _this.addChild(_this.m_containerObjs);
        var town_center = new egret.Bitmap(RES.getRes("town_center_png"));
        town_center.touchEnabled = false;
        town_center.x = 0;
        town_center.y = 0;
        town_center.anchorOffsetX = town_center.width / 2;
        town_center.anchorOffsetY = town_center.height / 2;
        town_center.scaleX = 0.3;
        town_center.scaleY = 0.3;
        _this.addChild(town_center);
        _this.CreateGrids();
        return _this;
        /*
              var spore0:egret.Bitmap = new egret.Bitmap( RES.getRes("candy1_png") );
                 spore0.touchEnabled = true;
                spore0.x = 30;
                spore0.y = 30;
                this.addChild( spore0 );
                */
    }
    CTown.prototype.CreateGrids = function () {
        var nWidth = CDef.s_nTileWidth;
        var nHeight = CDef.s_nTileHeight;
        var nHalfWidth = CDef.s_nTileWidth / 2;
        var nHalfHeight = CDef.s_nTileHeight / 2;
        for (var i = -10; i <= 10; i++) {
            for (var j = -5; j <= 5; j++) {
                var posX = 0;
                var posY = 0;
                if (i % 2 == 0) {
                    posX = j * nWidth;
                    posY = i * nHalfHeight;
                }
                else {
                    if (j > 0) {
                        posX = (j - 0.5) * nWidth;
                    }
                    else if (j < 0) {
                        posX = (j + 0.5) * nWidth;
                    }
                    else {
                        continue;
                    }
                    posY = i * nHalfHeight;
                }
                var grid = new CGrid();
                this.m_containerGrids.addChild(grid);
                grid.touchEnabled = true;
                grid.anchorOffsetX = nHalfWidth;
                grid.anchorOffsetY = CDef.s_nTileHeight;
                // grid.DrawRhombus( CDef.s_nTileWidth, CDef.s_nTileHeight, nHalfWidth, nHalfHeight);
                grid.x = posX;
                grid.y = posY;
                // j是x轴坐标，i是y轴坐标
                var szKey = j + "," + i;
                grid.SetText(szKey);
                grid.SetId(j, i);
                this.m_dicGrids[szKey] = grid;
                grid.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.handleTapGrid_Begin, this);
                grid.addEventListener(egret.TouchEvent.TOUCH_TAP, this.handleTapGrid, this);
            } // end for j
        } // end for i        
    }; // end CreateGrids
    CTown.prototype.GetGridByIndex = function (nIndexX, nIndexY) {
        var szKey = nIndexX + "," + nIndexY;
        var grid = this.m_dicGrids[szKey];
        return grid;
    };
    CTown.prototype.handleTapGrid_Begin = function (evt) {
        this.m_fTapBeginMouseX = evt.stageX;
        this.m_fTapBeginMouseY = evt.stageY;
    };
    CTown.prototype.handleTapGrid = function (evt) {
        if ((this.m_fTapBeginMouseX != evt.stageX) || (this.m_fTapBeginMouseY != evt.stageY)) {
            console.log("滑动了，不算点击：");
            return;
        }
        var the_target = evt.currentTarget;
        var grid = the_target;
        var dicId = grid.GetId();
        var nIndexX = dicId["nIndexX"];
        var nIndexY = dicId["nIndexY"];
        var tapped_grid = null;
        var bHit = grid.hitTestPoint(evt.stageX, evt.stageY, true);
        if (bHit) {
            // console.log( "点中：" + grid.GetId()["nIndexX"] + "_" + grid.GetId()["nIndexY"]  );
            tapped_grid = grid;
        }
        else {
            var nIndexX_ZuoShang = 0;
            if (nIndexY % 2 == 0) {
                nIndexX_ZuoShang = nIndexX;
            }
            else {
                nIndexX_ZuoShang = nIndexX - 1;
            }
            var nIndexY_ZuoShang = nIndexY - 1;
            grid = this.GetGridByIndex(nIndexX_ZuoShang, nIndexY_ZuoShang);
            if (grid != null) {
                bHit = grid.hitTestPoint(evt.stageX, evt.stageY, true);
                if (bHit) {
                    // console.log( "点中：" + grid.GetId()["nIndexX"] + "_" + grid.GetId()["nIndexY"]  );
                    tapped_grid = grid;
                }
                else {
                    var nIndexX_YouShang = 0;
                    var nIndexY_YouShang = nIndexY - 1;
                    if (nIndexY % 2 == 0) {
                        nIndexX_YouShang = nIndexX + 1;
                    }
                    else {
                        nIndexX_YouShang = nIndexX;
                    }
                    grid = this.GetGridByIndex(nIndexX_YouShang, nIndexY_YouShang);
                    if (grid != null) {
                        bHit = grid.hitTestPoint(evt.stageX, evt.stageY, true);
                        if (bHit) {
                            tapped_grid = grid;
                            //    console.log( "点中：" + grid.GetId()["nIndexX"] + "_" + grid.GetId()["nIndexY"]  );
                        }
                    }
                }
            }
        }
        if (tapped_grid != null) {
            console.log("点中：" + tapped_grid.GetId()["nIndexX"] + "_" + tapped_grid.GetId()["nIndexY"]);
        }
        else {
            console.log("什么都没点中");
        }
        /*
           var dic:Object = grid.GetPos();
           var obj:CObj = CObjManager.NewObjByObjFuncAndResId( Global.eObjFunc.road, 0 );
           obj.anchorOffsetX = obj.width / 2;
            obj.anchorOffsetY = obj.height;
           this.InsertObj( obj );
           obj.SetPos( dic["x"], dic["y"] );
           */
    };
    // 这里用“插入排序法”
    CTown.prototype.InsertObj = function (obj) {
        this.m_containerObjs.addChild(obj);
    };
    CTown.prototype.DragScene = function (fDeltaX, fDeltaY) {
        this.x += fDeltaX;
        this.y += fDeltaY;
    };
    return CTown;
}(egret.DisplayObjectContainer)); // end class CTown
__reflect(CTown.prototype, "CTown");
//# sourceMappingURL=CTown.js.map