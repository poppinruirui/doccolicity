var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var Global;
(function (Global) {
    var eObjType;
    (function (eObjType) {
        eObjType[eObjType["hori"] = 0] = "hori";
        eObjType[eObjType["vert"] = 1] = "vert";
    })(eObjType = Global.eObjType || (Global.eObjType = {}));
    var eObjFunc;
    (function (eObjFunc) {
        eObjFunc[eObjFunc["road"] = 0] = "road";
        eObjFunc[eObjFunc["building"] = 1] = "building";
        eObjFunc[eObjFunc["car"] = 2] = "car";
        eObjFunc[eObjFunc["plant"] = 3] = "plant";
    })(eObjFunc = Global.eObjFunc || (Global.eObjFunc = {}));
})(Global || (Global = {}));
var CObj = (function (_super) {
    __extends(CObj, _super);
    function CObj() {
        var _this = _super.call(this) || this;
        _this.m_bmpMainPic = new egret.Bitmap(); // 一个Obj的核心图片
        _this.m_objectPos = new Object();
        _this.addChild(_this.m_bmpMainPic);
        return _this;
    }
    CObj.prototype.SetObjType = function (eObjType) {
        this.m_eObjType = eObjType;
    };
    CObj.prototype.GetObjType = function () {
        return this.m_eObjType;
    };
    CObj.prototype.SetPos = function (x, y) {
        this.x = x;
        this.y = y;
    };
    CObj.prototype.GetPos = function () {
        this.m_objectPos["x"] = this.x;
        this.m_objectPos["y"] = this.y;
        return this.m_objectPos;
    };
    CObj.prototype.SetBitmap = function (bitmap) {
        this.m_bmpMainPic = bitmap;
    };
    CObj.prototype.SetTexture = function (tex) {
        this.m_bmpMainPic.texture = tex;
    };
    return CObj;
}(egret.DisplayObjectContainer)); // end class CObj
__reflect(CObj.prototype, "CObj");
//# sourceMappingURL=CObj.js.map